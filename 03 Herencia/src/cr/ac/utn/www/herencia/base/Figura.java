/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public abstract class Figura {

    protected Color color;

    public Figura() {
    }

    public Figura(Color color) {
        this.color = color;
    }

    public abstract void borra(Graphics g);

    public abstract void dibuja(Graphics g);

    public void mover(Graphics g) {
        borra(g);
        dibuja(g);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

}
