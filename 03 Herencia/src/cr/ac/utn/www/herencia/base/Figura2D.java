/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;

/**
 *
 * @author ALLAN
 */
public abstract class Figura2D extends Figura {

    protected Color colorRelleno;

    public Figura2D() {
    }

    public Figura2D(Color colorRelleno, Color color) {
        super(color);
        this.colorRelleno = colorRelleno;
    }

    public Color getColorRelleno() {
        return colorRelleno;
    }

    public void setColorRelleno(Color colorRelleno) {
        this.colorRelleno = colorRelleno;
    }

}
