/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Curva extends Figura1D {

    private double radio;

    public Curva() {
    }

    public Curva(double radio, int ptoIni, int ptoFin, Color color) {
        super(ptoIni, ptoFin, color);
        this.radio = radio;
    }

    @Override
    public void borra(Graphics g) {
        
    }

    @Override
    public void dibuja(Graphics g) {
        g.setColor(color);
        g.drawArc(ptoIni, ptoFin, (int) radio, (int) radio, 0, 90);
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

}
