/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;

/**
 *
 * @author ALLAN
 */
public abstract class Figura1D extends Figura {

    protected int ptoIni;
    protected int ptoFin;

    public Figura1D() {
    }

    public Figura1D(int ptoIni, int ptoFin, Color color) {
        super(color);
        this.ptoIni = ptoIni;
        this.ptoFin = ptoFin;
    }

    
    
    public int getPtoIni() {
        return ptoIni;
    }

    public int getPtoFin() {
        return ptoFin;
    }

    public void setPtoIni(int ptoIni) {
        this.ptoIni = ptoIni;
    }

    public void setPtoFin(int ptoFin) {
        this.ptoFin = ptoFin;
    }    

}
