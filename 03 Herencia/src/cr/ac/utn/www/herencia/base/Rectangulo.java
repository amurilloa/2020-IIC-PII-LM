/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Rectangulo extends Figura2D {

    private Punto ptoSupIzq;
    private Punto ptoInfDer;

    public Rectangulo() {
    }

    public Rectangulo(Punto ptoSupIzq, Punto ptoInfDer, Color colorRelleno, Color color) {
        super(colorRelleno, color);
        this.ptoSupIzq = ptoSupIzq;
        this.ptoInfDer = ptoInfDer;
    }

    @Override
    public void borra(Graphics g) {
        
    }

    @Override
    public void dibuja(Graphics g) {
        g.setColor(colorRelleno);
        int ancho = ptoInfDer.getX() - ptoSupIzq.getX();
        int alto = ptoInfDer.getY() - ptoSupIzq.getY();
        g.fillRect(ptoSupIzq.getX(), ptoSupIzq.getY(), ancho, alto);
        g.setColor(color);
        g.drawRect(ptoSupIzq.getX(), ptoSupIzq.getY(), ancho, alto);
    }


}
