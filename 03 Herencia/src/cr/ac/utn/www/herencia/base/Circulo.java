/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Circulo extends Figura2D {
    
    private Punto ptoCentro;
    private double radio;
    
    public Circulo() {
    }
    
    public Circulo(Punto ptoCentro, double radio, Color colorRelleno, Color color) {
        super(colorRelleno, color);
        this.ptoCentro = ptoCentro;
        this.radio = radio;
    }
    
    @Override
    public void borra(Graphics g) {
    }
    
    @Override
    public void dibuja(Graphics g) {
        int nx = (int) (ptoCentro.getX() - radio);
        int ny = (int) (ptoCentro.getY() - radio);
        int dia = (int) radio * 2;
        g.setColor(colorRelleno);
        g.fillOval(nx, ny, dia, dia);
        g.setColor(color);
        g.drawOval(nx, ny, dia, dia);
        
    }
    
    @Override
    public void mover(Graphics g) {
        System.out.println("Moviendo un Circulo");
        if(color == colorRelleno){
        return;
        }
        ptoCentro.setX(ptoCentro.getX() + 1);
        ptoCentro.setY(ptoCentro.getY() + 1);
        super.mover(g);
    }
    
}
