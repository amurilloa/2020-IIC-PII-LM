/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia.base;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Segmento extends Figura1D {
    
    public Segmento() {
    }
    
    public Segmento(int ptoIni, int ptoFin, Color color) {
        super(ptoIni, ptoFin, color);
    }
    
    @Override
    public void borra(Graphics g) {
        
    }
    
    @Override
    public void dibuja(Graphics g) {
        g.setColor(color);
        g.drawLine(ptoIni, 100, ptoFin, 100);
    }
    
    @Override
    public void mover(Graphics g) {
        System.out.println("Moviendo un Segmento");
        ptoIni++;
        ptoFin++;
        super.mover(g);
    }
    
}
