/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia;

import cr.ac.utn.www.herencia.base.Circulo;
import cr.ac.utn.www.herencia.base.Curva;
import cr.ac.utn.www.herencia.base.Figura;
import cr.ac.utn.www.herencia.base.Figura1D;
import cr.ac.utn.www.herencia.base.Punto;
import cr.ac.utn.www.herencia.base.Rectangulo;
import cr.ac.utn.www.herencia.base.Segmento;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

        Punto[] puntosArr = new Punto[2];
        
        ArrayList<Punto> puntos = new ArrayList<>();

        Punto p1 = new Punto(1, 12);
        Punto p2 = new Punto(1, 13);

        p1.equals(p2);
        puntos.add(p1);
        puntos.add(p2);

        puntosArr[0]=p1;
        puntosArr[1]=p2;
        System.out.println(puntos);
        System.out.println(Arrays.toString(puntosArr));
        
        
//        if (!puntos.contains(p2)) {
//            puntos.add(p2);
//        } else {
//            puntos.set(puntos.indexOf(p2), p2);
//        }

        int indice = puntos.indexOf(p2);
        if (indice == -1) {
            puntos.add(p2);
            System.out.println("P2 agregado");
        } else {
            puntos.set(indice, p2);
            System.out.println("Reemplaza el punto");
        }

        System.out.println("Tamaño de la lista: " + puntos.size());

//
//        Object[] objetos = new Object[10];
//
//        objetos[0] = "hola mundo";
//        objetos[1] = 11;
//        objetos[2] = false;
//        objetos[3] = new Curva();
//        objetos[4] = new Rectangulo();
//        objetos[5] = new Curva();
//
//        for (Object objeto : objetos) {
//            if (objeto instanceof Boolean) {
//                boolean temp = (boolean) objeto;
//                System.out.println(temp ? "Verdadero" : "Falso");
//            } else if (objeto instanceof String) {
//                System.out.println(((String) objeto).toUpperCase());
//            } else {
//                System.out.println(objeto);
//            }
//        }
//
//        Figura f = new Curva();
//        f.setColor(Color.RED);
//        Segmento s = new Segmento(100, 150, Color.yellow);
//        s.setPtoFin(200);
//
//        if (f instanceof Curva) {
//            ((Curva) f).setRadio(50);
//            System.out.println("Si es una instancia de curva");
//        } else if (f instanceof Figura1D) {
//            ((Figura1D) f).setPtoIni(100);
//            ((Figura1D) f).setPtoFin(200);
//            System.out.println("Si es una instancia de Figura1D");
//        } else if (f instanceof Figura) {
//            System.out.println("Si es una instancia de Figura");
//        }
//        Circulo c = new Figura();
//        Figura1D f1 = new Figura1D();
//        JFrame frm = new JFrame();
//        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        MiPanel pnl = new MiPanel();
//        frm.add(pnl);
//        frm.pack();
//        frm.setLocationRelativeTo(null);
//        frm.setVisible(true);
//        while (true) {
//            frm.repaint();
//            Thread.sleep(20);
//        }
    }
}
