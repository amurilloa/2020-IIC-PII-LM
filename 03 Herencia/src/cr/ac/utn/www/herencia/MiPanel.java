/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.www.herencia;

import cr.ac.utn.www.herencia.base.Circulo;
import cr.ac.utn.www.herencia.base.Curva;
import cr.ac.utn.www.herencia.base.Figura;
import cr.ac.utn.www.herencia.base.Figura1D;
import cr.ac.utn.www.herencia.base.Figura2D;
import cr.ac.utn.www.herencia.base.Punto;
import cr.ac.utn.www.herencia.base.Rectangulo;
import cr.ac.utn.www.herencia.base.Segmento;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel {

    private final LinkedList<Figura> figuras;

    public MiPanel() {
        figuras = new LinkedList<>();
        figuras.add(new Segmento(100, 200, Color.RED));
        figuras.add(new Segmento(250, 450, Color.BLUE));
        figuras.add(new Curva(100, 200, 50, Color.BLACK));
        figuras.add(new Circulo(new Punto(500, 100), 25, Color.ORANGE, Color.BLUE));
        figuras.add(new Circulo(new Punto(700, 100), 85, Color.BLUE, Color.BLUE));
        figuras.add(new Rectangulo(new Punto(100, 50), new Punto(200, 150), Color.DARK_GRAY, Color.RED));

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 200);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
        for (Figura figura : figuras) {
            figura.dibuja(g);
            System.out.println(figura.getClass());
        }
        for (Figura figura : figuras) {
            figura.mover(g);
        }        
    }

}
