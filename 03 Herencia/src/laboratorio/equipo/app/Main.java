/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.equipo.app;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import laboratorio.equipo.entidades.Entrenador;
import laboratorio.equipo.entidades.Futbolista;
import laboratorio.equipo.entidades.Masajista;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Futbolista f = new Futbolista(4, "Defensa", 206470762, "Allan", "Murillo Alfaro", LocalDate.parse("28/06/1988", df));
        System.out.println(f);

        Entrenador e = new Entrenador(506, 206470762, "Allan", "Murillo Alfaro", LocalDate.parse("28/06/1988", df));
        System.out.println(e);

        Masajista m = new Masajista(5, 206470762, "Allan", "Murillo Alfaro", LocalDate.parse("28/06/1988", df));
        System.out.println(m);
    }
}
