/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.equipo.entidades;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author ALLAN
 */
public class Masajista extends Persona {

    private int aniosExperiencia;

    public Masajista(int aniosExperiencia, int id, String nombre, String apellidos, LocalDate fechaNacimiento) {
        super(id, nombre, apellidos, fechaNacimiento);
        this.aniosExperiencia = aniosExperiencia;
    }

    public void darMasaje() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta dando un masaje");
    }

    public int getAniosExperiencia() {
        return aniosExperiencia;
    }

    public void setAniosExperiencia(int aniosExperiencia) {
        this.aniosExperiencia = aniosExperiencia;
    }

    @Override
    public String getInfo() {
        String data = "Cédula: %d\n"
                + "Nombre: %s %s\n"
                + "Fecha Nacimiento: %s\n"
                + "Edad: %d años\n"
                + "Experiencia: %d años";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format(data, id, nombre, apellidos, fc, getEdad(), aniosExperiencia);
    }

    @Override
    public String getData() {
        String data = "M,%d,%d,%s,%s,%s";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return String.format(data, aniosExperiencia, id, nombre, apellidos, fc);
    }

    @Override
    public String toString() {
        String str = String.format("%s %s (%d años exp)", nombre, apellidos, aniosExperiencia);
        return str;
    }

}
