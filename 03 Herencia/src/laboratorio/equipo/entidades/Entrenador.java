/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.equipo.entidades;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author ALLAN
 */
public class Entrenador extends Persona {

    private int idFederacion;

    public Entrenador(int idFederacion, int id, String nombre, String apellidos, LocalDate fechaNacimiento) {
        super(id, nombre, apellidos, fechaNacimiento);
        this.idFederacion = idFederacion;
    }

    public void dirigirPartido() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta dirigiendo el partido");
    }

    public void dirigirEntrenamiento() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta dirigiendo el entrenamiento");
    }

    public int getIdFederacion() {
        return idFederacion;
    }

    public void setIdFederacion(int idFederacion) {
        this.idFederacion = idFederacion;
    }

    @Override
    public String getInfo() {
        String data = "Cédula: %d\n"
                + "Nombre: %s %s\n"
                + "Fecha Nacimiento: %s\n"
                + "Edad: %d años\n"
                + "Federación: %d";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format(data, id, nombre, apellidos, fc, getEdad(), idFederacion);
    }

    @Override
    public String getData() { //E,506,206470762,Luis,Miranda Rojas,28/06/1988
        String data = "E,%d,%d,%s,%s,%s";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return String.format(data,idFederacion,id,nombre,apellidos,fc);
    }

    @Override
    public String toString() {
        String str = String.format("%s %s (Federación: %d)", nombre, apellidos, idFederacion);
        return str;
    }
}
