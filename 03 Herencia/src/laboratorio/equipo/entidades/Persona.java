/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.equipo.entidades;

import java.time.LocalDate;
import java.time.Period;

/**
 *
 * @author ALLAN
 */
public abstract class Persona {

    protected int id;
    protected String nombre;
    protected String apellidos;
    protected LocalDate fechaNacimiento;

    public Persona() {
    }

    public Persona(int id, String nombre, String apellidos, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
    }

    public void concentrarse() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta en concentración");
    }

    public void viajar() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta de viaje");
    }

    public int getEdad() {
        Period periodo = Period.between(fechaNacimiento, LocalDate.now());
        return periodo.getYears();
    }

    public abstract String getInfo();

    public abstract String getData();

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombre=" + nombre + ", apellidos="
                + apellidos
                //+ ", fechaNacimiento=" + fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + '}';
                + ", edad=" + getEdad() + '}';
    }

}
