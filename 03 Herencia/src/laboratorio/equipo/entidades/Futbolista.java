/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.equipo.entidades;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author ALLAN
 */
public class Futbolista extends Persona {

    private int dorsal;
    private String demarcacion;

    public Futbolista(int dorsal, String demarcacion, int id, String nombre, String apellidos, LocalDate fechaNacimiento) {
        super(id, nombre, apellidos, fechaNacimiento);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }

    public void jugarPartido() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta jugando el partido");
    }

    public void entrenar() {
        //TODO: Un método a nivel de entidades o lógica nunca debería imprimir
        System.out.println(".... " + nombre + " .... esta entrenando");
    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getDemarcacion() {
        return demarcacion;
    }

    public void setDemarcacion(String demarcacion) {
        this.demarcacion = demarcacion;
    }

    @Override
    public String toString() {
        String str = String.format("%s %s (%s - #%d)", nombre, apellidos, demarcacion, dorsal);
        return str;
    }

    public String getData() {//F,4,Defensa,206470762,Pedro,Mora Porras,28/06/2000
        String data = "F,%d,%s,%d,%s,%s,%s";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return String.format(data,dorsal,demarcacion,id,nombre,apellidos,fc);
    }

    @Override
    public String getInfo() {
        String data = "Cédula: %d\n"
                + "Nombre: %s %s\n"
                + "Fecha Nacimiento: %s\n"
                + "Edad: %d años\n"
                + "Dorsal: %d\n"
                + "Demarcación: %s";
        String fc = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        return String.format(data, id, nombre, apellidos, fc, getEdad(), dorsal, demarcacion);
    }

}
