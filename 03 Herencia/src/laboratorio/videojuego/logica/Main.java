/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.videojuego.logica;

import laboratorio.videojuego.entidades.Guerrero;
import laboratorio.videojuego.entidades.Mago;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Guerrero g = new Guerrero("Alfa", "AK-47", 150);
        Mago m = new Mago("Harry", "Quemar");
        System.out.println(g.combatir(15));
        System.out.println(g);
        System.out.println(m.encantar());
        System.out.println(m);
    }
}
