/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.videojuego.entidades;

/**
 *
 * @author ALLAN
 */
public class Guerrero extends Personaje {

    private String arma;

    public Guerrero(String nombre, String arma, int energia) {
        super(nombre, energia);
        this.arma = arma;
    }

    public String combatir(int energia) {
        this.energia -= energia;   
//        alimentarse(-energia);
//        setEnergia(getEnergia()-energia);
        return arma + " -" + energia;
    }

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }

    @Override
    public String toString() {
        return "Guerrero{" + "nombre=" + nombre + ", energia=" + energia + ", arma=" + arma + '}';
    }   
    
}
