/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.capas.bo;

import cr.ac.utn.capas.dao.UsuarioDAO;
import cr.ac.utn.capas.entities.Usuario;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author ALLAN
 */
public class UsuarioBO {

    public Usuario iniciarSesion(Usuario u) {

        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario es requerido");
        }

        if (u.getContrasena().isBlank()) {
            throw new RuntimeException("Contraseña es requerida");
        }

        if (u.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        //https://mirrors.ucr.ac.cr/apache//commons/codec/source/commons-codec-1.14-src.zip
        //Encriptar la contraseña 
        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));

        return new UsuarioDAO().iniciarSesion(u);
    }

}
