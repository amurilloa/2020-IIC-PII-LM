/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.capas.dao;

import cr.ac.utn.capas.entities.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ALLAN
 */
public class UsuarioDAO {

    public Usuario iniciarSesion(Usuario u) {
        //try with resourses
        try (Connection con = Conexion.getConexion()) {
            String sql = "select id,usuario,nombre,correo,contrasena,admin "
                    + " from veter.usuarios where usuario = ?"
                    + " and contrasena = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsuario());
            stm.setString(2, u.getContrasena());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                System.out.println("Encontró el usuario");
                return cargar(rs);
            }
        } catch (Exception e) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setNombre(rs.getString(3));
        u.setCorreo(rs.getString(4));
        u.setContrasena(rs.getString(5));
        u.setAdmin(rs.getBoolean(6));
        return u;
    }

}
