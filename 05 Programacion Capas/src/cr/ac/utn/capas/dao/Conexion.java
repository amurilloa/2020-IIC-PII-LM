/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.capas.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author ALLAN
 */
public class Conexion {

    private static final String DRIVER = "org.postgresql.Driver";
    private static final String TIPO = "jdbc:postgresql://";
    private static final String SERVER = "localhost";
    private static final String PUERTO = "5432";
    private static final String DB = "progra_dos";
    private static final String USER = "postgres";
    private static final String PASS = "postgres";

    public static Connection getConexion() throws Exception {
        Connection con = null;
        try {
            Class.forName(DRIVER);
            String strCon = String.format("%s%s:%s/%s", TIPO, SERVER, PUERTO, DB);
            con = DriverManager.getConnection(strCon, USER, PASS);
            return con;
        } catch (ClassNotFoundException ex) {
            System.out.println("Recuerde agregar la librería al proyecto");
            throw ex;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw ex;
        }
    }
    
//    public static void main(String[] args) throws Exception {
//        System.out.println(Conexion.getConexion());
//    }
}
