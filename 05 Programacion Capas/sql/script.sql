
-- drop table veter.usuarios

create table veter.usuarios(
	id serial primary key, 
	usuario text not null unique,
	nombre text not null,
	correo text not null unique,
	contrasena text not null,
	admin boolean default false
);

insert into veter.usuarios(usuario,nombre,correo,contrasena,admin) values('admin','Administrador','admin@allanmurillo.cr','ca2b46b4960815fa27f334a13299b552',true)
select * from veter.usuarios


create table veter.personas(
	id serial primary key, 
	cedula numeric not null unique,
	nombre text not null,
	apellido_uno text not null,
	apellido_dos text, 
	genero char default 'M', -- M: Masculino, F:Femenino
    correo text not null unique,
	fecha_nac date
);

-- Seleccionar registros 
select * from veter.personas order by id 
-- Crear registros 
insert into veter.personas(cedula, nombre, apellido_uno, apellido_dos, genero, correo, fecha_nac) 
values (206470762, 'Allan', 'Murillo', 'Alfaro', 'M', 'hola@allanmurillo.cr', '1988-06-28'), 
(206530948, 'Lineth', 'Matamoros', 'Fernández', 'F', 'lineth.matoros@gmail.com', '1988-11-17');
insert into veter.personas(cedula, nombre, apellido_uno, apellido_dos, genero, correo, fecha_nac) 
values (209920998, 'Lucía', 'Murillo', 'Matamoros', 'F', 'lucy@allanmurillo.cr', '2015-05-03')
--Actualizar Registros
update veter.personas set genero = 'M', correo = 'hola@allanmurillo.cr' where id = 1;
--Eliminar registros 
delete from veter.personas where id =1

drop table veter.telefonos

create table veter.telefonos(
	id serial primary key,
	telefono text not null,
	id_persona integer not null,
	constraint fk_tel_per foreign key (id_persona) references veter.personas(id)
)

insert into veter.telefonos(telefono,id_persona) values('8778-7857',2)
select * from veter.telefonos
-- alter table veter.telefonos add constraint fk_tel_per foreign key (id_persona) references veter.personas(id)


select id,usuario,nombre,correo,contrasena,admin  from veter.usuarios where usuario = ? and contrasena = ?




