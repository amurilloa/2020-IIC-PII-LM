/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.entities;

/**
 *
 * @author ALLAN
 */
public class Tipo {

    private int id;
    private String codigo;
    private String tipo;
    private boolean activo;

    public Tipo() {
    }

    public Tipo(int id, String codigo, String tipo, boolean activo) {
        this.id = id;
        this.codigo = codigo;
        this.tipo = tipo;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return String.format("%s - %s", codigo, tipo);
    }

}
