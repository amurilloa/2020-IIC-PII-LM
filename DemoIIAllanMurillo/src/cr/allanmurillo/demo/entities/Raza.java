/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.entities;

/**
 *
 * @author ALLAN
 */
public class Raza {
    
    private int id;
    private int idTipo;
    private Tipo tipo;
    private String codigo;
    private String raza;
    private boolean activo;

    public Raza() {
    }

    public Raza(int id, int idTipo, Tipo tipo, String codigo, String raza, boolean activo) {
        this.id = id;
        this.idTipo = idTipo;
        this.tipo = tipo;
        this.codigo = codigo;
        this.raza = raza;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s", codigo, raza);
    }
    
}
