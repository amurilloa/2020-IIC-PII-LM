/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.entities;

/**
 *
 * @author allanmurillo
 */
public class Usuario {

    private int id;
    private String usuario;
    private String nombre;
    private String correo;
    private String contrasena;
    private boolean admin;
    private boolean activo;

    public Usuario() {
        activo = true;
    }

    public Usuario(int id, String usuario, String nombre, String correo, String contrasena, boolean admin, boolean activo) {
        this.id = id;
        this.usuario = usuario;
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena = contrasena;
        this.admin = admin;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * Retorna el nombre del usuario y entre parentesis el usuario
     * @return String nombre(usuario)
     */
    public String getNombreUsu() {
        return String.format("%s(%s)", nombre, usuario);
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", usuario=" + usuario + ", nombre=" + nombre + ", correo=" + correo + ", contrasena=" + contrasena + ", admin=" + admin + ", activo=" + activo + '}';
    }

}
