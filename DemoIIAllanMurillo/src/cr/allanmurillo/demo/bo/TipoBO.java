/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.bo;

import cr.allanmurillo.demo.dao.TipoDAO;
import cr.allanmurillo.demo.entities.Raza;
import cr.allanmurillo.demo.entities.Tipo;
import java.util.ArrayList;

/**
 *
 * @author ALLAN
 */
public class TipoBO {

    public ArrayList<Tipo> cargarTipos() {
        return new TipoDAO().cargarTipos();
    }

    public ArrayList<Raza> cargarRazas(Tipo tipo) {
        if(tipo==null){
            throw  new RuntimeException("Favor seleccionar un Tipo");
        }
        
        return new TipoDAO().cargarRazas(tipo);
    }
    
}
