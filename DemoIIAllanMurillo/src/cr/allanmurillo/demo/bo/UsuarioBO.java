/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.bo;

import cr.allanmurillo.demo.dao.UsuarioDAO;
import cr.allanmurillo.demo.entities.Usuario;
import java.util.ArrayList;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Logica: Disminuir la mayor cantidad de llamadas a la capa de datos, filtrando
 * los posibles errores que se pueden detectar antes de invocar a la BD.
 *
 * @author ALLAN
 */
public class UsuarioBO {

    public Usuario iniciarSesion(Usuario u) {

        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario es requerido");
        }

        if (u.getContrasena().isBlank()) {
            throw new RuntimeException("Contraseña es requerida");
        }

        if (u.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        //https://mirrors.ucr.ac.cr/apache//commons/codec/source/commons-codec-1.14-src.zip
        //Encriptar la contraseña 
        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));

        return new UsuarioDAO().iniciarSesion(u);
    }

    public ArrayList<Usuario> getAll(String filtro) {
        return new UsuarioDAO().getAll(filtro);
    }

    public boolean guardar(Usuario u) {
        if (u.getUsuario().isBlank()) {
            throw new RuntimeException("Usuario es requerido");
        }
        if (u.getNombre().isBlank()) {
            throw new RuntimeException("Nombre es requerido");
        }
        if (u.getCorreo().isBlank()) {
            throw new RuntimeException("Correo es requerido");
        }

        if (u.getContrasena().isBlank()) {
            throw new RuntimeException("Contraseña es requerida");
        }

        if (u.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (u.getId() > 0) {
            return new UsuarioDAO().editar(u);
        } else {
            u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));
            return new UsuarioDAO().insertar(u);
        }

    }

    public void eliminar(Usuario sel) {
        if (sel.getId() > 0) {
            new UsuarioDAO().eliminar(sel);
        }
    }

    public void cambiarPass(Usuario u) {
        if (u.getContrasena().isBlank()) {
            throw new RuntimeException("Contraseña es requerida");
        }

        if (u.getContrasena().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }
        u.setContrasena(DigestUtils.md5Hex(u.getContrasena()));
        new UsuarioDAO().editar(u);
    }
}
