/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.dao;

/**
 *
 * @author ALLAN
 */
public class UtilDAO {

    public static String getMensaje(String code, char accion) {
        switch (code) {
            case "23503":
                return accion == 'E' ? "No se puede eliminar, tiene datos asociados" : "El dato no se ha podido relacionar";
            default:
                return "Sin conexión al servidor";
        }
    }

}
