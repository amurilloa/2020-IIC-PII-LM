/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.dao;

import cr.allanmurillo.demo.entities.Raza;
import cr.allanmurillo.demo.entities.Tipo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ALLAN
 */
public class TipoDAO {

    public ArrayList<Tipo> cargarTipos() {
        ArrayList<Tipo> tipos = new ArrayList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, tipo, activo FROM veter.tipos";
            PreparedStatement stm = con.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                tipos.add(cargarTipo(rs));
            }
            return tipos;
        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

    private Tipo cargarTipo(ResultSet rs) throws SQLException {
        Tipo t = new Tipo();
        t.setId(rs.getInt(1));
        t.setCodigo(rs.getString(2));
        t.setTipo(rs.getString(3));
        t.setActivo(rs.getBoolean(4));
        return t;
    }

    public ArrayList<Raza> cargarRazas(Tipo tipo) {
        ArrayList<Raza> razas = new ArrayList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, id_tipo, codigo, raza, activo FROM "
                    + " veter.razas where id_tipo = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, tipo.getId());

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                razas.add(cargarRaza(rs));
            }
            return razas;
        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

    private Raza cargarRaza(ResultSet rs) throws SQLException {

        Raza r = new Raza();
        r.setId(rs.getInt(1));
        r.setIdTipo(rs.getInt(2));
        r.setTipo(cargarTipoPorId(rs.getInt(2)));
        r.setCodigo(rs.getString(3));
        r.setRaza(rs.getString(4));
        r.setActivo(rs.getBoolean(5));
        return r;
    }

    private Tipo cargarTipoPorId(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, tipo, activo FROM veter.tipos where id = ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return cargarTipo(rs);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

}
