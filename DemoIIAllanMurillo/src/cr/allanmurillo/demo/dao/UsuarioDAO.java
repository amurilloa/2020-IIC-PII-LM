/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.allanmurillo.demo.dao;

import cr.allanmurillo.demo.entities.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ALLAN
 */
public class UsuarioDAO {

    public Usuario iniciarSesion(Usuario u) {
        //try with resourses
        try (Connection con = Conexion.getConexion()) {
            String sql = "select id,usuario,nombre,correo,contrasena,admin,activo "
                    + " from veter.usuarios where usuario = ?"
                    + " and contrasena = ? and activo";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsuario());
            stm.setString(2, u.getContrasena());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                System.out.println("Encontró el usuario");
                return cargar(rs);
            }
        } catch (Exception e) {
            throw new RuntimeException("Favor intente nuevamente");
        }
        return null;
    }

    public boolean insertar(Usuario u) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into veter.usuarios(usuario,nombre,correo, "
                    + " contrasena,admin, activo) values (?,?,?,?,?,?)";

            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsuario());
            stm.setString(2, u.getNombre());
            stm.setString(3, u.getCorreo());
            stm.setString(4, u.getContrasena());
            stm.setBoolean(5, u.isAdmin());
            stm.setBoolean(6, u.isActivo());

            return stm.executeUpdate() > 0;

        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

    public boolean editar(Usuario u) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update veter.usuarios set usuario=?, nombre=?, "
                    + " correo=?, contrasena=?, admin=?, activo=? WHERE id=?";

            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, u.getUsuario());
            stm.setString(2, u.getNombre());
            stm.setString(3, u.getCorreo());
            stm.setString(4, u.getContrasena());
            stm.setBoolean(5, u.isAdmin());
            stm.setBoolean(6, u.isActivo());
            stm.setInt(7, u.getId());

            return stm.executeUpdate() > 0;

        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

    public ArrayList<Usuario> getAll(String filtro) {
        ArrayList<Usuario> usuarios = new ArrayList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "select id,usuario,nombre,correo,contrasena,admin,activo "
                    + " from veter.usuarios ";

            if (!filtro.isBlank()) {
                sql += " where lower(usuario) like ?  or lower(nombre) like ?";
            }

            PreparedStatement stm = con.prepareStatement(sql);
            
            if (!filtro.isBlank()) {
                stm.setString(1, filtro.toLowerCase() + "%");
                stm.setString(2, "%" + filtro.toLowerCase() + "%");
            }

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                usuarios.add(cargar(rs));
            }
            return usuarios;
        } catch (Exception e) {
            throw new RuntimeException("Sin conexión al servidor");
        }
    }

    private Usuario cargar(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt(1));
        u.setUsuario(rs.getString(2));
        u.setNombre(rs.getString(3));
        u.setCorreo(rs.getString(4));
        u.setContrasena(rs.getString(5));
        u.setAdmin(rs.getBoolean(6));
        u.setActivo(rs.getBoolean(7));
        return u;
    }

    public void eliminar(Usuario u) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "delete from veter.usuarios WHERE id=?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, u.getId());
            stm.execute();
        } catch (Exception e) {
            String code = "";
            if (e instanceof SQLException) {
                code = ((SQLException) e).getSQLState();
                if (code.equals("23503")) {
                    u.setActivo(false);
                    editar(u);
                    return;
                }
            }
            throw new RuntimeException(UtilDAO.getMensaje(code, 'E'));
        }
    }

}
