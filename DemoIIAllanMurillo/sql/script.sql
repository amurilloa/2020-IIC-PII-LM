
-- drop table veter.usuarios

create table veter.usuarios(
	id serial primary key, 
	usuario text not null unique,
	nombre text not null,
	correo text not null unique,
	contrasena text not null,
	admin boolean default false
);

insert into veter.usuarios(usuario,nombre,correo,contrasena,admin) values('admin','Administrador','admin@allanmurillo.cr','ca2b46b4960815fa27f334a13299b552',true)
select * from veter.usuarios


create table veter.personas(
	id serial primary key, 
	cedula numeric not null unique,
	nombre text not null,
	apellido_uno text not null,
	apellido_dos text, 
	genero char default 'M', -- M: Masculino, F:Femenino
    correo text not null unique,
	fecha_nac date
);

-- Seleccionar registros 
select * from veter.personas order by id 
-- Crear registros 
insert into veter.personas(cedula, nombre, apellido_uno, apellido_dos, genero, correo, fecha_nac) 
values (206470762, 'Allan', 'Murillo', 'Alfaro', 'M', 'hola@allanmurillo.cr', '1988-06-28'), 
(206530948, 'Lineth', 'Matamoros', 'Fernández', 'F', 'lineth.matoros@gmail.com', '1988-11-17');
insert into veter.personas(cedula, nombre, apellido_uno, apellido_dos, genero, correo, fecha_nac) 
values (209920998, 'Lucía', 'Murillo', 'Matamoros', 'F', 'lucy@allanmurillo.cr', '2015-05-03')
--Actualizar Registros
update veter.personas set genero = 'M', correo = 'hola@allanmurillo.cr' where id = 1;
--Eliminar registros 
delete from veter.personas where id =2

drop table veter.telefonos

create table veter.telefonos(
	id serial primary key,
	telefono text not null,
	id_persona integer not null,
	constraint fk_tel_per foreign key (id_persona) references veter.personas(id)
)

insert into veter.telefonos(telefono,id_persona) values('8778-7857',2)
select * from veter.telefonos
-- alter table veter.telefonos add constraint fk_tel_per foreign key (id_persona) references veter.personas(id)
delete from veter.telefonos where id_persona = 2

select id,usuario,nombre,correo,contrasena,admin from veter.usuarios where usuario = 'admin' and contrasena = 'ca2b46b4960815fa27f334a13299b552'

select id,usuario,nombre,correo,contrasena,admin from veter.usuarios 

alter table veter.usuarios add column activo boolean default true

-- drop table veter.dummy
create table veter.dummy(
	id serial primary key,
	id_usuario integer not null,
	constraint fk_dum_usu foreign key (id_usuario) references veter.usuarios(id)
)

insert into veter.dummy(id_usuario) values(1),(2),(4)

select * from veter.dummy


select * from veter.usuarios where lower(usuario) like 'ad%'  or lower(nombre) like '%mata%'

CREATE TABLE veter.tipos
(
    id serial primary key,
    codigo text NOT NULL,
    tipo text NOT NULL,
    activo boolean DEFAULT true
);

CREATE TABLE veter.razas
(
    id serial primary key,
	id_tipo integer not null,
    codigo text NOT NULL,
    raza text NOT NULL,
    activo boolean DEFAULT true,
	constraint fk_raz_tip foreign key (id_tipo) references veter.tipos(id)
);

insert into veter.tipos(codigo, tipo) values('PE', 'Perros'), ('GA', 'Gatos')
select * from veter.tipos

insert into veter.razas(id_tipo,codigo,raza) values(1,'LA','Labrador'),(2,'AN','ANGORA'),(1,'PT','Pitbull')
select * from veter.razas where id_tipo = 2





