/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.demo.logica;

import cr.ac.utn.demo.ManejoArchivos;
import cr.ac.utn.demo.entidades.Raza;
import cr.ac.utn.demo.entidades.Tipo;
import java.util.LinkedList;

/**
 * W
 *
 * @author ALLAN
 */
public class Logica {

    private String usuLog;
    private final LinkedList<Tipo> tipos;
    private final ManejoArchivos db;

    private final static String RUTA_TIPOS = "datos/tipos/tipos.txt";
    private final static String RUTA_RAZAS = "datos/tipos/razas/%s.txt";

    public Logica() {
        tipos = new LinkedList<>();
        db = new ManejoArchivos();
        cargarTipos();
    }

    private void cargarTipos() {
        //Leer al archivo, sacar la persona
        String datos = db.leer(RUTA_TIPOS);
        System.out.println(datos);
        String[] lineas = datos.split("\n");
        for (String linea : lineas) {
            String[] aux = linea.split(",");
            Tipo t1 = new Tipo(aux[0], aux[1]);
            cargarRazas(t1);
            agregarTipo(t1);
        }
    }

    private void cargarRazas(Tipo tipo) {
        //Leer al archivo, sacar la persona
        String datos = db.leer(String.format(RUTA_RAZAS, tipo.getCod()));
        System.out.println(datos);
        String[] lineas = datos.split("\n");
        for (String linea : lineas) {
            String[] aux = linea.split(",");
            Raza r1 = new Raza(aux[0], aux[1]);
            tipo.agregarRaza(r1);
        }

    }

    public void guardarTipos() {
        String datos = "";
        for (Tipo tipo : tipos) {
            datos += tipo.getData() + "\n";
            guardarRazas(tipo);
        }
        db.escribir(RUTA_TIPOS, datos.trim());
    }

    private void guardarRazas(Tipo tipo) {
        String datos = "";
        for (Raza raza : tipo.getRazas()) {
            datos += raza.getData() + "\n";
        }
        db.escribir(String.format(RUTA_RAZAS, tipo.getCod()), datos.trim());
    }

    public boolean agregarTipo(Tipo tipo) {
        //Reviso que no exista
        if (tipos.contains(tipo)) {
            return false;
        }
        //Lo agrego y retorno el resultado
        boolean guardar = tipos.add(tipo);
        if (guardar) {
            guardarTipos();
        }
        return guardar;
    }

    public boolean eliminarTipo(Tipo tipo) {
        return tipos.remove(tipo);
    }

    public boolean editarTipo(Tipo tNuevo, Tipo tViejo) {
        int pos = tipos.indexOf(tViejo);
        if (tipos.remove(tViejo)) {
            tipos.add(pos, tNuevo);
            return true;
        }
        return false;
    }

    public boolean iniciarSesion(String usu, String pas) {
        if (usu.equals("admin") && pas.equals("123")) {
            usuLog = usu;
            return true;
        }
        return false;
    }

    public LinkedList<Tipo> getTipos() {
        return tipos;
    }

    public String getUsuLog() {
        return usuLog;
    }
}
