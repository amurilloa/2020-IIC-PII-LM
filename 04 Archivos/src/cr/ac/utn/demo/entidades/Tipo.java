/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.demo.entidades;

import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author ALLAN
 */
public class Tipo {

    private String cod; //PE,GA,PC
    private String tipo;
    private LinkedList<Raza> razas;

    public Tipo() {
        razas = new LinkedList<>();
    }

    public Tipo(String cod, String tipo) {
        razas = new LinkedList<>();
        this.cod = cod;
        this.tipo = tipo;
    }

    public void agregarRaza(Raza raza) {
        razas.add(raza);
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public LinkedList<Raza> getRazas() {
        return razas;
    }

    public void setRazas(LinkedList<Raza> razas) {
        this.razas = razas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tipo other = (Tipo) obj;
        if (!Objects.equals(this.cod, other.cod)) {
            return false;
        }
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", tipo, cod);//(PE)Perro
    }

    public String getData() {
        return String.format("%s,%s", cod, tipo);//PE,PERROS
    }

}
