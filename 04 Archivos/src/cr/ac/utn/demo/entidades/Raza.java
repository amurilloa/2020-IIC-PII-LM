/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.utn.demo.entidades;

/**
 *
 * @author ALLAN
 */
public class Raza {

    private String cod;
    private String raza;

    public Raza() {
    }

    public Raza(String cod, String raza) {
        this.cod = cod;
        this.raza = raza;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return String.format("(%s)%s", cod, raza);

    }

    public String getData() {
        return String.format("%s,%s", cod, raza);//PA,PASTOR ALEMAN
    }

}
