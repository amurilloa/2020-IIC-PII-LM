/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg01.relaciones.entre.clases;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private Coche coche;
    private Corazon corazon;

    public Persona(String nombre) {
        corazon = new Corazon();
        this.nombre = nombre;
    }

    public void setCoche(Coche c) {
        this.coche = c;
    }

    public void viaja() {
    }

    public void emociona() {
        corazon.setRitmo(corazon.getRitmo() + 2);
    }

    public void tranquiliza() {
        if (corazon.getRitmo() > 70) {
            corazon.setRitmo(corazon.getRitmo() - 2);
        }

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", coche=" + coche + ", corazon=" + corazon + '}';
    }

}
