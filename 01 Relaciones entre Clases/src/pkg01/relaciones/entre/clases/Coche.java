/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg01.relaciones.entre.clases;

/**
 *
 * @author ALLAN
 */
public class Coche {

    private Persona conductor;
    private Motor motor;
    
    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void enciende() {
    }

    public void apaga() {
    }

    public void acelera() {
    }

    public void frena() {
    }

    public void isEncendido() {
    }

    @Override
    public String toString() {
        return "Coche{" + "conductor=" + conductor + ", motor=" + motor + '}';
    }

    
}
