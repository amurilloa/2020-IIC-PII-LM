/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg01.relaciones.entre.clases;

/**
 *
 * @author ALLAN
 */
public class Corazon {

    private int ritmo;

    public Corazon() {
        ritmo = 90;
    }
    

    public void setRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    public int getRitmo() {
        return ritmo;
    }

    @Override
    public String toString() {
        return "Corazon{" + "ritmo=" + ritmo + '}';
    }
}
